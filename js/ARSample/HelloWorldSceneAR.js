"use strict";

import React, { Component } from "react";

import { StyleSheet } from "react-native";

import {
  ViroARScene,
  ViroText,
  ViroMaterials,
  ViroBox,
  Viro3DObject,
  ViroAmbientLight,
  ViroSpotLight,
  ViroARPlane,
  ViroARPlaneSelector,
  ViroQuad,
  ViroNode,
  ViroAnimations,
  ViroConstants,
  ViroImage,
  ViroScene,
  ViroCamera
} from "react-viro";

var createReactClass = require("create-react-class");

var HelloWorldSceneAR = createReactClass({
  getInitialState() {
    return {
      hasARInitialized: false,
      text: "Initializing AR..."
    };
  },
  render: function() {
    return (
      <ViroARScene>
        <ViroCamera position={[0, 0, 0]} active={true}>
          <ViroImage
            position={[0, 0, -2]}
            scale={[1, 1, 1]}
            // height={1}
            // width={1}
            placeholderSource={require("./res/gigi_lingua_de_fora.png")}
            source={require("./res/gigi_lingua_de_fora.png")}
          />
        </ViroCamera>
      </ViroARScene>
    );
  },
  _onTrackingUpdated(state, reason) {
    if (
      !this.state.hasARInitialized &&
      state == ViroConstants.TRACKING_NORMAL
    ) {
      this.setState({
        hasARInitialized: true,
        text: "A GIRAFA VAI APARECER!"
      });
    }
  }
});

var styles = StyleSheet.create({
  helloWorldTextStyle: {
    fontFamily: "Arial",
    fontSize: 30,
    color: "#ffffff",
    textAlignVertical: "center",
    textAlign: "center"
  }
});

ViroMaterials.createMaterials({
  grid: {
    diffuseTexture: require("./res/grid_bg.jpg")
  }
});

ViroAnimations.registerAnimations({
  rotate: {
    properties: {
      rotateY: "+=90"
    },
    duration: 250 //.25 seconds
  }
});

module.exports = HelloWorldSceneAR;
